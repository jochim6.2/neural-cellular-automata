import numpy
import pygame
from itertools import combinations_with_replacement

class Interface():
        running = True
        grid = None
        ticks = 60
        scale = 10 #10 pixels font 1
        clock = None



        def __init__(self):
                pygame.init()
                pygame.display.set_mode((0, 0), pygame.FULLSCREEN)
                pygame.display.set_caption('cellular')
                pygame.display.flip()
                self.clock = pygame.time.Clock()

        def run(self):
                while self.running:
                        self.clock.tick(self.ticks)

                        for event in pygame.event.get():

                                if event.type == pygame.QUIT:
                                        self.running = False

                                elif event.type == pygame.KEYDOWN:
                                        if event.key  == pygame.K_ESCAPE:
                                                self.running = False

        def get_screen_size(self, with_scale = True):
                display = pygame.display.Info()
                return with_scale and display.current_w/self.scale, display.current_h/self.scale or display.current_w, display.current_h

        def set_grid(self,x,y,color):
                self.grid[x][y].fill(color)

        def get_gridl(self,x,y):
                return self.screen[x][y]

        def update_screen(self):
               for i , row in enumerate(self.grid):
                       for j, surface in enumerate(row):
                               pygame.display.blit(surface, (i * self.scale, j * self.scale))
               pygame.display.update()

        def _init_grid(self):
                self.grid = numpy.full(self.get_screen_size(), pygame.Surface(self.scale, self.scale))

if __name__ == "__main__" :
        Interface()