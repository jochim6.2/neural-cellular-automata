import interface
class Main:
    interface = interface.Interface()

    def __init__(self):
        self.run()

    def run(self):
        self.interface.run()
        self.interface._init_grid()

if __name__ == "__main__":
    Main()